20软件工程4班 202010414412 马有付

# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

### 实验步骤

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

查询员工工资高于20000的员工信息和所在部门信息

查询1：

```SQL
sql ：
set autotrace on
SELECT employees.employee_id, employees.first_name, employees.last_name, departments.department_name
FROM employees
INNER JOIN departments ON employees.department_id = departments.department_id
WHERE employees.salary >20000;
输出结果：

Predicate Information (identified by operation id):
---------------------------------------------------

   4 - access("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
       filter("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
   5 - filter("EMPLOYEES"."SALARY">20000)


统计信息
----------------------------------------------------------
	191  recursive calls
	  0  db block gets
	369  consistent gets
	 11  physical reads
	  0  redo size
	795  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	 14  sorts (memory)
	  0  sorts (disk)
	  1  rows processed



```

- 查询2

```SQL
sql：
set autotrace on
SELECT employees.employee_id, employees.first_name, employees.last_name, departments.department_name
FROM employees, departments
WHERE employees.department_id = departments.department_id
AND employees.salary > 20000;

输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------

   4 - access("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
       filter("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
   5 - filter("EMPLOYEES"."SALARY">20000)


统计信息
----------------------------------------------------------
	 66  recursive calls
	  0  db block gets
	 47  consistent gets
	  0  physical reads
	  0  redo size
	795  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	  1  rows processed

```

## 实验分析

根据以上的统计信息的consistent gets字段信息。我们能够发现，查询语句2效率高一些。

## 实验注意事项

- 完成时间：2023-3-21，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test1目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
