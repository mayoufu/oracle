-- 创建水果分类表
CREATE TABLE FRUIT_CATEGORY
(
  id   INTEGER,
  name VARCHAR2(50)
) TABLESPACE fruit_data;

-- 创建水果表
CREATE TABLE FRUIT
(
  id          INTEGER,
  name        VARCHAR2(50),
  category_id INTEGER,
  price       NUMBER(10, 2),
  stock       INTEGER
) TABLESPACE fruit_data;

-- 创建客户表
CREATE TABLE CUSTOMER
(
  id      INTEGER,
  name    VARCHAR2(50),
  phone   VARCHAR2(20),
  address VARCHAR2(100)
) TABLESPACE fruit_data;

-- 创建订单表
CREATE TABLE FRUIT_ORDER
(
  id         INTEGER,
  customer_id INTEGER,
  fruit_id    INTEGER,
  quantity    INTEGER,
  order_date  DATE
) TABLESPACE fruit_data;

-- 创建用户
CREATE USER fruit_admin IDENTIFIED BY admin_password;
GRANT CONNECT, RESOURCE, DBA TO fruit_admin;

CREATE USER fruit_user IDENTIFIED BY user_password;
GRANT CONNECT, RESOURCE TO fruit_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON FRUIT_CATEGORY TO fruit_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON FRUIT TO fruit_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON CUSTOMER TO fruit_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON FRUIT_ORDER TO fruit_user;