DECLARE
  -- 声明变量
  order_id NUMBER;
  total_amount NUMBER;
  order_seq_value NUMBER;
BEGIN

  
  -- 调用存储过程：计算订单总金额
  total_amount := myORDER.calculate_total_amount(order_id);
  
  -- 输出结果
  DBMS_OUTPUT.PUT_LINE('订单ID：' || order_id);
  DBMS_OUTPUT.PUT_LINE('订单总金额：' || total_amount);
END;
/
