-- 插入数据
DECLARE
  v_product_name VARCHAR2(50);
  v_sale_price VARCHAR2(50);
  v_import_price VARCHAR2(50);
  v_num NUMBER;
BEGIN
  FOR i IN 1..20000 LOOP
    -- 生成随机的货物名称
    SELECT '水果名称' || TO_CHAR(DBMS_RANDOM.VALUE(1, 1000)) INTO v_product_name FROM DUAL;
    
    -- 生成随机的售卖价格
    SELECT TRUNC(DBMS_RANDOM.VALUE(2, 500), 2) INTO v_sale_price FROM DUAL;
    
    -- 计算批发价格
    v_import_price := v_sale_price * 0.6;
    
    -- 生成随机的剩余数量
    SELECT TRUNC(DBMS_RANDOM.VALUE(0, 100)) INTO v_num FROM DUAL;
    
    -- 执行插入操作
    INSERT INTO TB_COMMODITY (PRODUCT_NAME, SALE_PRICE, IMPORT_PRICE, NUM)
    VALUES (v_product_name, i, v_sale_price, v_import_price, v_num);
  END LOOP;
  
  COMMIT;
END;
/

select * from TB_COMMODITY







