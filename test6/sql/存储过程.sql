

-- 创建程序包
CREATE OR REPLACE PACKAGE fruit_pkg AS
  PROCEDURE add_fruit(p_name VARCHAR2, p_category_id INTEGER, p_price NUMBER, p_stock INTEGER);
  FUNCTION get_fruit_stock(p_fruit_id INTEGER) RETURN INTEGER;
  PROCEDURE create_order(p_customer_id INTEGER, p_fruit_id INTEGER, p_quantity INTEGER);
  FUNCTION get_order_total(p_order_id INTEGER) RETURN NUMBER;
END fruit_pkg;
/

-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY fruit_pkg AS
  PROCEDURE add_fruit(p_name VARCHAR2, p_category_id INTEGER, p_price NUMBER, p_stock INTEGER) AS
  BEGIN
    INSERT INTO FRUIT (id, name, category_id, price, stock)
    VALUES (fruit_seq.NEXTVAL, p_name, p_category_id, p_price, p_stock);
  END add_fruit;

  FUNCTION get_fruit_stock(p_fruit_id INTEGER) RETURN INTEGER AS
    v_stock INTEGER;
  BEGIN
    SELECT stock INTO v_stock FROM FRUIT WHERE id = p_fruit_id;
    RETURN v_stock;
  END get_fruit_stock;

  PROCEDURE create_order(p_customer_id INTEGER, p_fruit_id INTEGER, p_quantity INTEGER) AS
  BEGIN
    INSERT INTO FRUIT_ORDER (id, customer_id, fruit_id, quantity, order_date)
    VALUES (order_seq.NEXTVAL, p_customer_id, p_fruit_id, p_quantity, SYSDATE);
  END create_order;

  FUNCTION get_order_total(p_order_id INTEGER) RETURN NUMBER AS
    v_total NUMBER;
  BEGIN
    SELECT SUM(quantity * price) INTO v_total
    FROM FRUIT_ORDER fo
    JOIN FRUIT f ON fo.fruit_id = f.id
    WHERE fo.id = p_order_id;
    RETURN v_total;
  END get_order_total;
END fruit_pkg;
/

