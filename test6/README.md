﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

实验6（期末考核） 基于Oracle数据库的水果销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

## 1.全部代码实现（每部分都有注解）：

\-1. 创建表空间

CREATE TABLESPACE fruit_data

 DATAFILE 'fruit_data.dbf'

 SIZE 100M

 AUTOEXTEND ON;



![pict1](./pict1.png)



CREATE TABLESPACE fruit_index

 DATAFILE 'fruit_index.dbf'

 SIZE 100M

 AUTOEXTEND ON;



--2. 创建水果分类表

CREATE TABLE FRUIT_CATEGORY

(

 id  INTEGER,

 name VARCHAR2(50)

) TABLESPACE fruit_data;

![pict2](./pict2.png)

-- 3.创建水果表

CREATE TABLE FRUIT

(

 id      INTEGER,

 name     VARCHAR2(50),

 category_id INTEGER,

 price    NUMBER(10, 2),

 stock    INTEGER

) TABLESPACE fruit_data;

![pict3](./pict3.png)

-- 4.创建客户表

CREATE TABLE CUSTOMER

(

 id    INTEGER,

 name   VARCHAR2(50),

 phone  VARCHAR2(20),

 address VARCHAR2(100)

) TABLESPACE fruit_data;



-- 5.创建订单表

CREATE TABLE FRUIT_ORDER

(

 id     INTEGER,

 customer_id INTEGER,

 fruit_id   INTEGER,

 quantity   INTEGER,

 order_date  DATE

) TABLESPACE fruit_data;

![pict3](./pict3.png)

-- 6.创建用户

CREATE USER fruit_admin IDENTIFIED BY admin_password;

GRANT CONNECT, RESOURCE, DBA TO fruit_admin;



CREATE USER fruit_user IDENTIFIED BY user_password;

GRANT CONNECT, RESOURCE TO fruit_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON FRUIT_CATEGORY TO fruit_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON FRUIT TO fruit_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON CUSTOMER TO fruit_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON FRUIT_ORDER TO fruit_user;



-- 7.创建程序包

CREATE OR REPLACE PACKAGE fruit_pkg AS

 PROCEDURE add_fruit(p_name VARCHAR2, p_category_id INTEGER, p_price NUMBER, p_stock INTEGER);

 FUNCTION get_fruit_stock(p_fruit_id INTEGER) RETURN INTEGER;

 PROCEDURE create_order(p_customer_id INTEGER, p_fruit_id INTEGER, p_quantity INTEGER);

 FUNCTION get_order_total(p_order_id INTEGER) RETURN NUMBER;

END fruit_pkg;

/

![pict4](./pict4.png)

-- 8.创建程序包体

CREATE OR REPLACE PACKAGE BODY fruit_pkg AS

 PROCEDURE add_fruit(p_name VARCHAR2, p_category_id INTEGER, p_price NUMBER, p_stock INTEGER) AS

 BEGIN

  INSERT INTO FRUIT (id, name, category_id, price, stock)

  VALUES (fruit_seq.NEXTVAL, p_name, p_category_id, p_price, p_stock);

 END add_fruit;

![pict5](./pict5.png)

 FUNCTION get_fruit_stock(p_fruit_id INTEGER) RETURN INTEGER AS

  v_stock INTEGER;

 BEGIN

  SELECT stock INTO v_stock FROM FRUIT WHERE id = p_fruit_id;

  RETURN v_stock;

 END get_fruit_stock;



 PROCEDURE create_order(p_customer_id INTEGER, p_fruit_id INTEGER, p_quantity INTEGER) AS

 BEGIN

  INSERT INTO FRUIT_ORDER (id, customer_id, fruit_id, quantity, order_date)

  VALUES (order_seq.NEXTVAL, p_customer_id, p_fruit_id, p_quantity, SYSDATE);

 END create_order;



 FUNCTION get_order_total(p_order_id INTEGER) RETURN NUMBER AS

  v_total NUMBER;

 BEGIN

  SELECT SUM(quantity * price) INTO v_total

  FROM FRUIT_ORDER fo

  JOIN FRUIT f ON fo.fruit_id = f.id

  WHERE fo.id = p_order_id;

  RETURN v_total;

 END get_order_total;

END fruit_pkg;

/



-- 9.数据库备份方案

-- 1. 使用RMAN进行全备份

RMAN> CONFIGURE CONTROLFILE AUTOBACKUP ON;

RMAN> CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '/backup/%F';

RMAN> CONFIGURE DEVICE TYPE DISK PARALLELISM 1 BACKUP TYPE TO BACKUPSET;

RMAN> CONFIGURE DEFAULT DEVICE TYPE TO DISK;

RMAN> CONFIGURE RETENTION POLICY TO REDUNDANCY 1;

RMAN> BACKUP DATABASE PLUS ARCHIVELOG;



-- 2. 使用Data Pump进行逻辑备份

expdp fruit_admin/admin_password@db_name DIRECTORY=backup_dir DUMPFILE=fruit_backup.dmp LOGFILE=fruit_backup.log SCHEMAS=fruit_admin,fruit_user

/

![pict6](./pict6.png)

三.实验收获与总结

该项目的实验过程可能会遇到一些挑战和问题，比如数据完整性、数据备份、性能优化和系统安全等。但同时也能带来一些很好的收获和总结，包括：

1. 数据库设计与优化：在数据库设计方面，本项目需要学会如何进行数据模型设计，如何管理数据库表、索引和视图等，以及如何优化查询性能，以提高系统的可用性和稳定性。
2. 数据管理与备份：本项目的重点在于数据管理和备份。在这个过程中，我们获得了一定的实践经验，在数据备份、恢复和保护方面学习到了很多方法。
3. 售前售后服务与用户体验：另一方面，该系统管理过程中还涉及到了用户体验与售前售后服务，这使得研发团队需要更好地了解市场需求，必须管理好公司的客户数据，以便提供更好更精准的服务。

总的来说，基于Oracle数据库的水果销售系统的设计项目使我们对数据库技术、数据管理以及优化等方面的能力有了更深入的了解。如果这些挑战和问题能够得到适当的处理，那么在项目的实施中就能够获得更好的收获和优势。

